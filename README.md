BACKLOG

- Abstracao de pessoa, por ID, imagem e "presença (?)"
- Conseguir comparar imagens capturadas com imagens salvas anteriormente
- Separar em: "função de cadastro" e "função de pesquisar por face"
- Adaptar o algoritmo para funcionar em server
- "Conseguir separar lista de presencas em diferentes dias/momentos" (?)

JÁ FEITO

- Funcao que detecta as faces
- Funcao que salva imagem caso desejado (e possível)

POSSÍVEIS MELHORIAS 

key := window.WaitKey(1)

// se só há uma face detectada, permite o salvamento da imagem
if len(rects) == 1 {
			
	fmt.Println("Press 's' to save the image")
	SaveImage(img, key)
			
}

TENTAR COLOCAR "key := window.WaitKey(1)" DENTRO DO IF, PARA TESTE